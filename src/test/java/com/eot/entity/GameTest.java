package com.eot.entity;

import com.eot.utils.InputReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class GameTest {

    private InputReader inputReader = Mockito.mock(InputReader.class);

    private Game game;

    @Before
    public void init() {
        game = new Game(2, new ConsolePlayer(inputReader), new ConsolePlayer(inputReader), new ArrayList<>());
    }

    @Test
    public void should_return_correct_score_after_one_turn() {
        game = new Game(1, new ConsolePlayer(inputReader), new ConsolePlayer(inputReader), new ArrayList<>());

        Mockito.when(inputReader.readLine())
                .thenReturn("ch");
        game.start();
        Assert.assertEquals(0, game.getPlayers().get(0).getScore());
        Assert.assertEquals(0, game.getPlayers().get(1).getScore());
    }

    @Test
    public void should_return_correct_score_after_two_turn() {

        Mockito.when(inputReader.readLine())
                .thenReturn("co")
                .thenReturn("co")
                .thenReturn("ch")
                .thenReturn("co");
        game.start();
        Assert.assertEquals(5, game.getPlayers().get(0).getScore());
        Assert.assertEquals(1, game.getPlayers().get(1).getScore());
    }

    @Test
    public void should_return_correct_score_console_always_cheat() {
        game = new Game(1, new ConsolePlayer(inputReader), new CheatPlayer(), new ArrayList<>());

        Mockito.when(inputReader.readLine())
                .thenReturn("co");

        game.start();
        Assert.assertEquals(-1, game.getPlayers().get(0).getScore());
        Assert.assertEquals(3, game.getPlayers().get(1).getScore());
    }


    @Test
    public void should_return_correct_score_cheat_always_cheat() {
        game = new Game(3, new CheatPlayer(), new CheatPlayer(), new ArrayList<>());

        game.start();
        Assert.assertEquals(0, game.getPlayers().get(0).getScore());
        Assert.assertEquals(0, game.getPlayers().get(1).getScore());
    }


    @Test
    public void should_return_correct_score_always_cooperate_cheat() {
        game = new Game(5, new CooperatePlayer(), new CheatPlayer(), new ArrayList<>());

        game.start();
        Assert.assertEquals(-5, game.getPlayers().get(0).getScore());
        Assert.assertEquals(15, game.getPlayers().get(1).getScore());
    }

    @Test
    public void should_return_correct_score_always_cooperate_copycat() {
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer();
        List<Listener> listeners = new ArrayList<>();
        listeners.add(copyCatPlayer);
        game = new Game(2, new ConsolePlayer(inputReader), copyCatPlayer, listeners);

        Mockito.when(inputReader.readLine())
                .thenReturn("ch", "ch");

        game.start();
        Assert.assertEquals(3, game.getPlayers().get(0).getScore());
        Assert.assertEquals(-1, game.getPlayers().get(1).getScore());
    }

    @Test
    public void should_return_correct_score_console_detective() {
        DetectivePlayer detectivePlayer = new DetectivePlayer();
        List<Listener> listeners = new ArrayList<>();
        listeners.add(detectivePlayer);
        game = new Game(5, new ConsolePlayer(inputReader), detectivePlayer, listeners);

        Mockito.when(inputReader.readLine())
                .thenReturn("co", "co", "co", "co","ch");

        game.start();
        Assert.assertEquals(5, game.getPlayers().get(0).getScore());
        Assert.assertEquals(9, game.getPlayers().get(1).getScore());
    }


}
