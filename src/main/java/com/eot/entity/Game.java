package com.eot.entity;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private List<Player> players = new ArrayList<>();
    private Machine machine;

    private List<Listener> listeners;
    private int rounds;

    public Game(int rounds, Player p1, Player p2, List<Listener> listeners) {
        this.machine = new Machine();
        this.listeners = listeners;
        this.rounds = rounds;
        players.add(p1);
        players.add(p2);
    }

    public void start() {
        for (int i = 0; i < rounds; i++) {
            Move p1 = players.get(0).getMove();
            Move p2 = players.get(1).getMove();

            for (Listener listener : listeners) {
                if (listener == players.get(0)) {
                    System.out.println("updating last move p1");
                    listener.updateLastMove(p2);
                } else {
                    System.out.println("updating last move p2");
                    listener.updateLastMove(p1);
                }
            }

            ScoreCard scoreCard = machine.getScores(p1, p2);
            players.get(0).updateScore(scoreCard.getScore1());
            players.get(1).updateScore(scoreCard.getScore2());
            System.out.println(" Score \n Player 1:" +
                    players.get(0).getScore() +
                    " -- Player 2:" +
                    players.get(1).getScore());
        }

    }

    public List<Player> getPlayers() {
        return players;
    }
}
