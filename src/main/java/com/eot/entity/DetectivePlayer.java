package com.eot.entity;

import java.util.Stack;

public class DetectivePlayer implements Player, Listener {

    private Move move;
    private int score;
    private boolean isOpponentCheated = false;

    private Stack<Move> firstFourMoves = new Stack<>();

    {
        firstFourMoves.push(Move.COOPERATE);
        firstFourMoves.push(Move.CHEAT);
        firstFourMoves.push(Move.COOPERATE);
        firstFourMoves.push(Move.COOPERATE);
    }

    @Override
    public void updateLastMove(Move lastMove) {
         if(lastMove == Move.CHEAT && !this.firstFourMoves.isEmpty()){
             this.isOpponentCheated = true;
         }
         if(this.isOpponentCheated){
             move = lastMove;
         } else {
             move = Move.CHEAT;
         }
    }

    @Override
    public Move getMove() {
        if (!firstFourMoves.isEmpty()) {
            return firstFourMoves.pop();
        }
        return move;
    }

    @Override
    public void updateScore(int score) {
        this.score += score;
    }

    @Override
    public int getScore() {
        return score;
    }
}
