package com.eot.entity;

public class CopyCatPlayer implements Player , Listener {
    private int score;
    private Move otherPlayerLastMove = Move.COOPERATE;

    @Override
    public Move getMove() {
        return otherPlayerLastMove;
    }

    public void updateLastMove(Move lastMove) {
        this.otherPlayerLastMove = lastMove;
    }

    @Override
    public void updateScore(int score) {
        this.score += score;
    }

    @Override
    public int getScore() {
        return score;
    }
}
