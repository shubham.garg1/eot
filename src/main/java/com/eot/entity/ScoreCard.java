package com.eot.entity;

public class ScoreCard {
    private Integer score1;
    private Integer score2;

    public ScoreCard(int score1, int score2) {
        this.score1 = score1;
        this.score2 =score2;
    }
    public int getScore1() {
        return score1;
    }

    public int getScore2() {
        return  score2;
    }


}
