package com.eot;

import com.eot.entity.ConsolePlayer;
import com.eot.entity.CopyCatPlayer;
import com.eot.entity.Game;
import com.eot.entity.Listener;
import com.eot.utils.InputReader;
import com.eot.utils.InputReaderImpl;

import java.util.ArrayList;
import java.util.List;

public class EotApplication {

    public static void main(String args[]) {
        InputReader inputReader = new InputReaderImpl();
        int rounds = 2;
        CopyCatPlayer p2 = new CopyCatPlayer();
        List<Listener> listeners = new ArrayList<>();
        listeners.add(p2);
        Game game = new Game(rounds, new ConsolePlayer(inputReader), p2, listeners);
        game.start();
    }
}
