package com.eot.utils;

public interface InputReader {
    public String readLine();
}
