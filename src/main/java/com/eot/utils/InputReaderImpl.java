package com.eot.utils;

import java.util.Scanner;

public class InputReaderImpl implements InputReader {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String readLine() {
        return scanner.nextLine();
    }
}
